package com.titima.equatic

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.titima.equatic.databinding.FragmentBigBinding
import com.titima.equatic.databinding.FragmentLittleBinding


class Big_Fragment : Fragment() {
    private var binding: FragmentBigBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentBigBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            bigFragment= this@Big_Fragment
        }
    }

    fun goTonextSpebig(){
        findNavController().navigate(R.id.action_big_Fragment_to_spebigFragment)
    }
    fun goTonextSpebigtwo(){
        findNavController().navigate(R.id.action_big_Fragment_to_spebigtwoFragment)
    }
    fun goTonextTurtlespe(){
        findNavController().navigate(R.id.action_big_Fragment_to_turtlebigspeFragment)
    }
    fun goTonextStingray(){
        findNavController().navigate(R.id.action_big_Fragment_to_spestingrayFragment)
    }
}