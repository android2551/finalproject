package com.titima.equatic

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.titima.equatic.databinding.FragmentJellFishBinding
import com.titima.equatic.databinding.FragmentSpebigtwoBinding

class JellFishFragment : Fragment() {
        private var binding: FragmentJellFishBinding? = null

        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val fragmentBinding = FragmentJellFishBinding.inflate(inflater, container, false)
            binding = fragmentBinding
            return fragmentBinding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            binding?.apply {
                jellfishFragment = this@JellFishFragment
            }
        }
    }