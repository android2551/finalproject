package com.titima.equatic

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.titima.equatic.databinding.FragmentLittleBinding

class Little_Fragment : Fragment() {
    private var binding: FragmentLittleBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentLittleBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            littleFragment= this@Little_Fragment
        }
    }

    fun goTonextcartoon(){
        findNavController().navigate(R.id.action_little_Fragment_to_spelittleFragment)
    }
    fun goTonextstarfish(){
        findNavController().navigate(R.id.action_little_Fragment_to_starfishFragment)
    }
    fun goTonextjellfish(){
        findNavController().navigate(R.id.action_little_Fragment_to_jellFishFragment)
    }
    fun goTonextseahores(){
        findNavController().navigate(R.id.action_little_Fragment_to_seahorseFragment)
    }
}