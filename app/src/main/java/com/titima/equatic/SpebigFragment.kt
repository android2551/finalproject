package com.titima.equatic

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.titima.equatic.databinding.FragmentSpebigBinding
import com.titima.equatic.databinding.FragmentSpelittleBinding

class SpebigFragment : Fragment() {
    private var binding: FragmentSpebigBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentSpebigBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            spebigFragment = this@SpebigFragment
        }
    }
}