package com.titima.equatic

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.titima.equatic.databinding.FragmentSpelittleBinding

class SpelittleFragment : Fragment() {
    private var binding: FragmentSpelittleBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentSpelittleBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            spelittleFragment = this@SpelittleFragment
        }
    }

//    fun goTonextScreen(){
//        findNavController().navigate(R.id.action_mainFragment_to_little_Fragment2)
//    }

}