package com.titima.equatic

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.titima.equatic.databinding.FragmentBigBinding.inflate
import com.titima.equatic.databinding.FragmentTurtlebigspeBinding

class TurtlebigspeFragment : Fragment() {
    private var binding: FragmentTurtlebigspeBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentTurtlebigspeBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            turtlebigspeFragment = this@TurtlebigspeFragment
        }
    }
}